#!/bin/bash

umask 0002
currentDir=`pwd`

nohup java -DLOG_DIR=$currentDir -DCONFIG_PATH=$currentDir -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005 -jar deliverables/automationCleanUpUtility.jar &

if [ "$?" -ne "0" ]; then
	exit 1
fi