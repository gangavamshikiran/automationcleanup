#!/bin/bash

umask 0002
currentDir=`pwd`

nohup java -DLOG_DIR=$currentDir -DCONFIG_PATH=$currentDir -jar deliverables/automationCleanUpUtility.jar &

if [ "$?" -ne "0" ]; then
	exit 1
fi