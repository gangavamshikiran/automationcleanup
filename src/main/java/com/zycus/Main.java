package com.zycus;

import com.zycus.constant.EnvEnum;
import com.zycus.constant.TenantEnum;
import com.zycus.util.ConsulUtil;
import com.zycus.util.DataSourceUtil;
import com.zycus.util.QueryHelper;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;
import java.util.Properties;

import static com.zycus.util.QueryHelper.executeBatchQueries;

@Slf4j
public class Main
{
	public static        String      ENV;
	public static        String      CONSUL_IP_PORT;
	private static final QueryParser invQueries             = new QueryParser();
	private static final QueryParser poQueries              = new QueryParser();
	private static final QueryParser catalogQueries         = new QueryParser();
	private static       EnvEnum     currentEnv;
	public static        String      CONFIG_PATH            = System.getProperty("CONFIG_PATH");
	private static final String      CONFIG_PROPERTIES_FILE = "config.properties";
	private static       int         invBatchSize           = -1;
	private static       int         poBatchSize            = -1;
	private static       int         catalogBatchSize       = -1;

	static
	{
		init();
	}

	private static void init()
	{
		log.info("Initializing the utility");
		checkEnvVariables();
		loadQueries();
		loadingTheCurrentEnvironmentData();
		ConsulUtil.initConsulUtil();
		DataSourceUtil.initDataSourceUtil();
		log.info("Initializing successful");
		log.info("Running the utility for {} setup", ENV);

	}

	private static void loadingTheCurrentEnvironmentData()
	{
		log.info("Loading {} setup data", ENV);
		try
		{
			currentEnv = EnvEnum.valueOf(ENV);
		}
		catch (Exception exception)
		{
			log.error("Something went wrong while loading {} setup data. Please check the logs and re-run the utility",
					ENV);
			log.debug("Exception in loading {} setup data", ENV, exception);
			System.exit(-1);
		}
	}

	private static void checkEnvVariables()
	{
		Properties appConfig = new Properties();
		String appConfigPropertyPath = CONFIG_PATH + File.separator + CONFIG_PROPERTIES_FILE;
		log.debug("config.properties path set as {} ", appConfigPropertyPath);
		try
		{
			appConfig.load(new FileInputStream(appConfigPropertyPath));
		}
		catch (IOException e)
		{
			log.error(
					"Something went wrong while reading config.properties. Please check the logs and re-run the utility");
			log.debug("Exception in reading config.properties ", e);
			System.exit(-1);
		}

		log.info("Checking required variables [ SETUP_ENV , CONSUL_IP_PORT ]");
		if (!appConfig.containsKey("SETUP_ENV") || "".equals(appConfig.getProperty("SETUP_ENV")))
		{
			log.error("'SETUP_ENV' is not set in config.properties. Please set and rerun the utility [ {} ]", ENV);
			System.exit(-1);
		}
		else
		{
			ENV = appConfig.getProperty("SETUP_ENV");
			log.info("'SETUP_ENV' is set as [ {} ]", ENV);
		}

		if (!appConfig.containsKey("CONSUL_IP_PORT") || "".equals(appConfig.getProperty("CONSUL_IP_PORT")))
		{
			log.info("'CONSUL_IP_PORT' not set. Using localhost:8500 as default");
			CONSUL_IP_PORT = "localhost:8500";
		}
		else
		{
			CONSUL_IP_PORT = appConfig.getProperty("CONSUL_IP_PORT");
			log.info("'CONSUL_IP_PORT' is set as [ {} ]", CONSUL_IP_PORT);
		}
		if (appConfig.containsKey("PO_BATCH_SIZE") && !"".equals(appConfig.getProperty("PO_BATCH_SIZE")))
		{
			poBatchSize = Integer.parseInt(appConfig.getProperty("PO_BATCH_SIZE"));
			log.info("'PO_BATCH_SIZE' is set as {}", poBatchSize);
		}
		if (appConfig.containsKey("INV_BATCH_SIZE") && !"".equals(appConfig.getProperty("INV_BATCH_SIZE")))
		{
			invBatchSize = Integer.parseInt(appConfig.getProperty("INV_BATCH_SIZE"));
			log.info("'INV_BATCH_SIZE' is set as {}", invBatchSize);
		}
		if (appConfig.containsKey("CATALOG_BATCH_SIZE") && !"".equals(appConfig.getProperty("CATALOG_BATCH_SIZE")))
		{
			catalogBatchSize = Integer.parseInt(appConfig.getProperty("CATALOG_BATCH_SIZE"));
			log.info("'CATALOG_BATCH_SIZE' is set as {}", catalogBatchSize);
		}
	}

	private static void loadQueries()
	{
		log.info("Loading required queries");

		try
		{
			poQueries.read(new InputStreamReader(
					Objects.requireNonNull(Main.class.getClassLoader().getResourceAsStream("query/po_queries.xml"))));
			invQueries.read(new InputStreamReader(
					Objects.requireNonNull(Main.class.getClassLoader().getResourceAsStream("query/invoice_queries.xml"))));
			catalogQueries.read(new InputStreamReader(Objects.requireNonNull(
					Main.class.getClassLoader().getResourceAsStream("query/catalog_queries.xml"))));
		}
		catch (Exception exception)
		{
			log.error("Something went wrong while loading queries. Please check the logs and re-run the utility");
			log.debug("Exception in loading queries", exception);
			System.exit(-1);
		}
		log.info("Required queries loaded successfully");
	}

	public static void main(String[] args)
	{
		TenantEnum[] tenantEnumArray = currentEnv.getTenants();
		int noOfTenant = tenantEnumArray.length;
		log.info("{} tenants configured for automation cleanup for {} setup", noOfTenant, ENV);

		for (int i = 0; i < noOfTenant; i++)
		{
			TenantEnum currentTenant = tenantEnumArray[i];
			log.info("{}/{} Starting cleanup for {}", i + 1, noOfTenant, currentTenant);
			if (QueryHelper.validate(currentTenant))
			{
				executeBatchQueries(poQueries, currentTenant, "PoQueries", poBatchSize);
				executeBatchQueries(invQueries, currentTenant, "InvoiceQueries", invBatchSize);
//				executeBatchQueries(catalogQueries, currentTenant, "CatalogQueries", catalogBatchSize);
			}
			else
			{
				log.info("Skipping cleanup {} tenant ", currentTenant);
			}
		}
		log.info("automation cleanup for {} setup successful", ENV);

	}
}
