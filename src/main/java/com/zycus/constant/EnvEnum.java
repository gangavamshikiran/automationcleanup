package com.zycus.constant;

import lombok.Getter;

import static com.zycus.constant.TenantEnum.*;

@Getter
public enum EnvEnum
{
//	DEV(DEV__TEST);

	QCVMWARE(QCVM_CODECEPT_AUTOMATION),

	QCNEW(QMS_SANITY),

	RMPARTNER(RM_ZCS),

	PARTNER(PARTNER_ZCS),

	STAGINGVMWARE(STAGING_SANITY, AUSUAT_QMS_SANITY),

	PROD(USPROD_ZCS, UKPROD_SANITY6, SGPROD_ZCS, AUSPROD_ZCS);

	private final TenantEnum[] tenants;

	EnvEnum(TenantEnum... tenants)
	{
		this.tenants = tenants;
	}

}
