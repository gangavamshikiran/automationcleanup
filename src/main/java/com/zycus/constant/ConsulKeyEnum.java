package com.zycus.constant;

import lombok.Getter;

@Getter
public enum ConsulKeyEnum
{
	CATALOG_POSTGRES_URL, CATALOG_POSTGRES_USERNAME, CATALOG_POSTGRES_PASSWORD
}
