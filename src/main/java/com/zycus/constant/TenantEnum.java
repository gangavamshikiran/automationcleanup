package com.zycus.constant;

import lombok.Getter;

@Getter
public enum TenantEnum
{
//QC
	QCVM_CODECEPT_AUTOMATION("901a571e-70f7-4ae1-9a38-b4bba3d45814"),
//QM
	QMS_SANITY("b5675501-65c1-4eb8-b4c3-7bfd924848b9"),
//RM
	RM_ZCS("732b3fc1-687b-42ac-b71f-d755fe90de04"),
//Partner
	PARTNER_ZCS("732b3fc1-687b-42ac-b71f-d755fe90de04"),
//Staging
	STAGING_SANITY ("3e5c881f-19d1-4d4c-b729-78a388d35914"),
	AUSUAT_QMS_SANITY("b5675501-65c1-4eb8-b4c3-7bfd924848b9"),
//Production
	USPROD_ZCS("732b3fc1-687b-42ac-b71f-d755fe90de04"),

	UKPROD_SANITY6("22451818-f7e9-4fdb-8ace-6f78420fa942"),

	SGPROD_ZCS("a4122cb8-b007-474d-81cc-5715f6d0d976"),

	AUSPROD_ZCS("bf7343e7-97d2-4f7f-a2cf-d2f79c0af553");

	private final String   id;
//	removed created_by list
//	private final String[] userIdList;

	TenantEnum(String id)
	{
		this.id = id;
		//	removed created_by list
//		this.userIdList = userIdList;
	}

}
