package com.zycus.util;

import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;

@Slf4j
public class DataSourceUtil
{
	public static final String JDBC_URL_KEY    = "CATALOG_POSTGRES_URL";
	public static final String DB_USERNAME_KEY = "CATALOG_POSTGRES_USERNAME";
	public static final String DB_PASSWORD_KEY = "CATALOG_POSTGRES_PASSWORD";
	public static final String dbDriver        = "org.postgresql.Driver";
	private static      String jdbcURL;
	private static      String username;
	private static      String password;

	public static void initDataSourceUtil()
	{
		try
		{
			Class.forName(dbDriver);
			jdbcURL = ConsulUtil.getValue(JDBC_URL_KEY);
			username = ConsulUtil.getValue(DB_USERNAME_KEY);
			password = PasswordProtectorUtil.decode(ConsulUtil.getValue(DB_PASSWORD_KEY));
		}
		catch (Exception exception)
		{
			log.info("Unable to connect to database. Please check the logs and re-run the utility");
			log.debug("Exception in connecting database", exception);
			System.exit(-1);
		}

	}

	public static Connection getJdbcConnection()
	{
		Connection conn = null;
		try
		{
			conn = DriverManager.getConnection(jdbcURL, username, password);
			if (conn == null)
			{
				log.info("Unable to connect to database. Please check the logs and re-run the utility");
				System.exit(-1);
			}

		}
		catch (Exception exception)
		{
			log.info("Unable to connect to database. Please check the logs and re-run the utility");
			log.debug("Exception in connecting database", exception);
			System.exit(-1);
		}
		return conn;
	}
}
