package com.zycus.util;

import com.google.common.collect.ImmutableMap;
import com.orbitz.consul.Consul;
import com.orbitz.consul.KeyValueClient;
import com.zycus.constant.ConsulKeyEnum;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

import static com.zycus.Main.CONSUL_IP_PORT;
import static com.zycus.Main.ENV;

@Slf4j
public class ConsulUtil
{
	private static final String                       PRODUCT = "ZSN";
	private static       KeyValueClient               kvClient;
	private static       ImmutableMap<String, String> keyValue;

	public static void initConsulUtil()
	{
		log.info("Initializing consul client");
		String consulUrl = "http://";
		try
		{
			consulUrl += CONSUL_IP_PORT;
			kvClient = Consul.builder().withUrl(consulUrl).build().keyValueClient();
			keyValue = getKeyValueMap();
		}
		catch (Exception exception)
		{
			log.error(
					"Something went wrong while initializing consul client. Please check the logs and re-run the utility");
			log.debug("Exception in initializing consul", exception);
			System.exit(-1);
		}

	}

	private static ImmutableMap<String, String> getKeyValueMap() throws Exception
	{
		Map<String, String> keyValue = new HashMap<>();

		log.debug("Consul fetched keys and values");
		for (ConsulKeyEnum consulKeyEnum : ConsulKeyEnum.values())
		{
			String key = String.format("/%s/%s/%s", ENV, PRODUCT, consulKeyEnum.name());
			String value = kvClient.getValuesAsString(key).get(0);
			keyValue.put(consulKeyEnum.name(), value);

			log.debug("key:[ {} ] and value:[ {} ]", key, value);
		}

		return ImmutableMap.copyOf(keyValue);
	}

	public static String getValue(String key)
	{
		if (keyValue.containsKey(key))
			return keyValue.get(key);
		else
			throw new IllegalArgumentException(key + "not found");
	}

}
